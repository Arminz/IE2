// Import required java libraries
//import com.opencsv.CSVReader;
//import com.opencsv.bean.CsvToBeanBuilder;

import com.opencsv.CSVReader;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

// Extend HttpServlet class
public class HelloWorld extends HttpServlet {

   public void init() throws ServletException {
      // Do required initialization
   }

   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
      
      // Set response content type
      response.setContentType("text/html");

      String relativeWebPath = "/WEB-INF/static/grades.csv";
      String absoluteDiskPath  = getServletContext().getRealPath(relativeWebPath);
//
//
      Reader r = new FileReader(absoluteDiskPath);

      CSVReader csv = new CSVReader(r);
//
      String page = "<table>\n";

      for (String[] row : csv.readAll())
      {
         page += "<tr>\n";
         for (String column : row)
         {
            page += "<th>" + column + "</th>\n";
//            System.out.println(column);
         }
         page += "</tr>\n";
//         System.out.println();
      }
      page += "</table>\n";



      // Actual logic goes here.
      PrintWriter out = response.getWriter();
//      String page =
//         "<table>\n"
//            +"<tr>\n"
//               +"<th>Student-No</th>"
//               +"<th>Name</th>\n"
//               +"<th>Grade</th>\n"
//            +"</tr>\n"
//            +"<tr>\n"
//               +"<td>810199101</td>\n"
//               +"<td>Ghamar</td>\n"
//               +"<td>12.5</td>\n"
//            +"</tr>\n"
//            +"<tr>\n"
//               +"<td>810199102</td>\n"
//               +"<td>Ghamar</td>\n"
//               +"<td>17.3</td>\n"
//            +"</tr>\n"
//            +"<tr>\n"
//               +"<td>810199103</td>\n"
//               +"<td>Ghodrat</td>\n"
//               +"<td>8.5</td>\n"
//            +"</tr>\n"
//         +"</table>";
      out.println(page);
   }

   public void destroy() {
      // do nothing.
   }
}
