cp ./static/* ../apache-tomcat-9.0.4/webapps/ROOT/WEB-INF/static/

javac -cp .:../apache-tomcat-9.0.4/lib/servlet-api.jar:../apache-tomcat-9.0.4/lib/opencsv-4.1.jar:../apache-tomcat-9.0.4/lib/commons-csv-1.5.jar ./src/*.java -d ../apache-tomcat-9.0.4/webapps/ROOT/WEB-INF/classes/

#cp ./out/*.class ../apache-tomcat-9.0.4/webapps/ROOT/WEB-INF/classes/

../apache-tomcat-9.0.4/bin/catalina.sh stop

../apache-tomcat-9.0.4/bin/catalina.sh start
